Title: Hello, world
Date: 2019-04-16
Category: GitLab
Tags: pelican, gitlab
Slug: hello-world

Hello, world.

Hailing from GitLab Pages! See <https://pages.gitlab.io> for more info.

This space will be for me to store random bits of info that I don't want to forget.
If they're useful to others, good for them.
