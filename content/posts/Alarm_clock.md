Title: Alarm Clock: Project overview
Date: 2019-11-02
Category: Projects
Tags: rpi, rust, clock
Slug: alarm-clock-overview

## Background

Ever since I was born, I had to wake up (and eventually get up, but those events do not necessarily happen at the same time) at specific times, for various reasons, that usually were related to school and later work. Until I was able to tell time, my parents were my alarm clock. Then, I got a little battery-powered analog clock. As it was all during elementary school, I only needed a basic device, since all school days started at the same hour of the day.

The problems with this alarm were the following:

  * Need to set it manually every night.
  * Hard to read when it's dark. The hands were vaguely fluorescent, but still.
  * The alarm itself is a standard "beep beep beep", couldn't be customized
  * No form of memory, notion of day of the week, etc...

When I got to collège (~ middle school), I got a digital radio alarm clock, with some fancy features. I *think* it could have several presets according the day of the week, it had a snooze button, and a whole lot of buttons I never used. And of course I could listen to FM radio in my room, which seemed awesome at the time.

However, this alarm clock has several key disadvantages:

  * No offline memory configuration. If the power went out, all the config went *poof*, even the time of the day, even if the interruption was short.
  * The time display was awful : black characters on a yellow-ish background, and *backlit*. I think it was continuously on, too, because I distinctly remember having the alarm clock facing the wall instead of my bed.
  * The choice in alarms was restricted to either a *beeep* or whatever was on the radio.

And after that, my alarm clock was provided by my phone,
which gave me presets, battery power, custom sounds. Yay.

Except it still isn't right.

  * The device doesn't stay on-site. It's really easy to leave your phone in the living room and thus not hear the alarm.
  * You have to leave your phone on all night.
  * You have to make sure you have enough battery left (yes, my phone holds more than one day of charge so I don't charge it every night)
  * It makes it easy to do stuff on your phone before and after sleeping. I hear that's a bad thing.
  * I haven't found an alarm app that does everything I want it to (and I'm too lazy to write one from scratch)

So, now I want to make an alarm clock :).

## The hardware

I have a Raspberry Pi 2B lying around somewhere in my flat. For the first version it'll do the trick, however to minimize power consumption I might buy a Pi Zero or see if there are other boards like that.

I'm going to need a 7-segment display for the Pi, some speakers (ideally that wouldn't need their own power supply), and at least one button to stop the alarm. A second one would be nice when we're going to want to make the device configurable. This second button would only be there to enable network communication.

## The software

### First version

The first version will simply be a daemon with a hardcoded sound and time, and will sound the alarm on every day at that time, and stop it when the button is pushed. Simple, easy.

### Second version

The second version will expose a HTTP REST API to set the alarm time.

### After that...

After that, I'm going to work on implementing features as I feel the need for them. Presets are nice to have. Multi-user (playing on the stereo aspect) would be really cool to have, so that my partner and I can both use the alarm without waking the other. Of course, a nicer UI than `curl` to configure the alarm will be needed. Being able to upload new files to use as alarm via this UI would be nice. Having the system choose an alarm at random would be cool. Maybe plug into Spotify or Youtube Music or Deezer ? I see when I'll get there.